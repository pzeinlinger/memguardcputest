package main

import (
	"encoding/hex"
	"log"
	"net/http"

	"github.com/awnumar/memguard"
)

func main() {
	e := memguard.NewEnclaveRandom(32)
	err := http.ListenAndServe(":5050", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		b, _ := e.Open()
		defer b.Destroy()
		w.WriteHeader(200)
		w.Write([]byte(hex.EncodeToString(b.Bytes())))
	}))
	if err != nil {
		log.Println(err)
	}
}
